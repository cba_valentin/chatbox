var express = require("express");
var app = express();
var http = require('http').Server(app);
var jade = require("jade");
var io = require("socket.io").listen(http);

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set("view options", { layout: false });
// app.configure(function() {
    app.use(express.static(__dirname + '/public'));
// });
app.get('/', function(req, res){
  res.render('home.jade');
});
http.listen(3000, function(){
  console.log('listening on *:3000');
});

io.sockets.on('connection', function (socket) {

	socket.on('setgroup', function (data){
		socket.group = data
	});

    socket.on('setPseudo', function (data) {
	   socket.pseudo = data;
	});

	socket.on('message', function (message) {
		var data = { 'message' : message, "pseudo" : socket.pseudo, "group" : socket.group };
		socket.broadcast.emit('message', data);
	    // socket.pseudo = function (error, name) {
	    //     var data = { 'message' : message, pseudo : name };
	    //     socket.broadcast.emit('message', data);
	    //     console.log("user " + name + " send this : " + message);
	    // }
	});
});
