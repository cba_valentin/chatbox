var socket = io.connect();
function addMessage(msg, pseudo) {
    $("#chatEntries").append('<div class="message"><p>' + pseudo + ' : ' + msg + '</p></div>');
}

function sentMessage() {
    if ($('#messageInput').val() != "") 
    {
        socket.emit('message', $('#messageInput').val());
        addMessage($('#messageInput').val(), "Me", new Date().toISOString(), true);
        $('#messageInput').val('');
    }
}

function setGroup() {
	if($("#groupInput").val() != "")
	{
		socket.emit("setgroup", $("#groupInput").val());
		$('#chatControls').show();
		$('#groupInput').hide();
        $('#groupSet').hide();
	}
}

function setPseudo() {
    if ($("#pseudoInput").val() != "")
    {
        socket.emit('setPseudo', $("#pseudoInput").val());
        $('#groupInput').show();
        $('#groupSet').show();
        $('#pseudoInput').hide();
        $('#pseudoSet').hide();
    }
}

socket.on('message', function(data) {
	if(data["group"] == $("#groupInput").val()){
    	addMessage(data['message'], data['pseudo']);
    }
});

$(function() {
    $("#chatControls").hide();
    $('#groupInput').hide();
    $('#groupSet').hide();
    $("#pseudoSet").click(function() {setPseudo()});
    $("#submit").click(function() {sentMessage();});
    $("#groupSet").click(function() {setGroup();});
});